package api

import (
	"net/http"

	"firestream/api/internal/logic/api"
	"firestream/api/internal/svc"
	"firestream/api/internal/types"
	"github.com/zeromicro/go-zero/rest/httpx"
)

func GetApiByIdHandler(svcCtx *svc.ServiceContext) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var req types.IDReq
		if err := httpx.Parse(r, &req); err != nil {
			httpx.ErrorCtx(r.Context(), w, err)
			return
		}

		l := api.NewGetApiByIdLogic(r.Context(), svcCtx)
		resp, err := l.GetApiById(&req)
		if err != nil {
			httpx.ErrorCtx(r.Context(), w, err)
		} else {
			httpx.OkJsonCtx(r.Context(), w, resp)
		}
	}
}
