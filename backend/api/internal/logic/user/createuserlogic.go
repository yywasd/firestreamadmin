package user

import (
	"context"

	"firestream/api/internal/svc"
	"firestream/api/internal/types"
	"firestream/utils/encrypt"
	"firestream/utils/pointy"

	"firestream/utils/dberrorhandler"
	"firestream/utils/i18n"

	"github.com/zeromicro/go-zero/core/logx"
)

type CreateUserLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
}

func NewCreateUserLogic(ctx context.Context, svcCtx *svc.ServiceContext) *CreateUserLogic {
	return &CreateUserLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
	}
}

func (l *CreateUserLogic) CreateUser(req *types.UserInfo) (resp *types.BaseMsgResp, err error) {
	data, err := l.svcCtx.CoreRpc.CreateUser(l.ctx, &coreclient.CreateUserReq{
		Username: req.Username,
		Password: encrypt.EncryptPassword(req.Password),
		Nickname: req.Nickname,
		Mobile:   req.Mobile,
		Email:    req.Email,
		Avatar:   req.Avatar,
		Gender:   pointy.Int32(req.Gender),
		Birthday: pointy.Int64(req.Birthday),
	})
	if err != nil {
		return nil, err
	}

	return &types.BaseMsgResp{
		Msg: data.Msg,
	}, nil
}
