package user

import (
	"context"

	"firestream/api/internal/svc"
	"firestream/api/internal/types"
	"firestream/ent"
	"firestream/utils/dberrorhandler"
	"firestream/utils/entx"
	"firestream/utils/uuidx"

	"github.com/zeromicro/go-zero/core/logx"
)

type UpdateUserLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
}

func NewUpdateUserLogic(ctx context.Context, svcCtx *svc.ServiceContext) *UpdateUserLogic {
	return &UpdateUserLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
	}
}

func (l *UpdateUserLogic) UpdateUser(req *types.UserInfo) (resp *types.BaseMsgResp, err error) {
	err = entx.WithTx(l.ctx, l.svcCtx.DB, func(tx *ent.Tx) error {
		query := tx.User.UpdateOneID(uuidx.ParseUUIDString(*req.Id)).
			SetNickname(*req.Nickname).
			SetAvatar(*req.Avatar).
			SetEmail(*req.Email)

		if req.Password != nil {
			query.SetPassword(*req.Password)
		}

		if req.RoleIds != nil {
			err = tx.User.UpdateOneID(uuidx.ParseUUIDString(*req.Id)).ClearRoles().Exec(l.ctx)
			if err != nil {
				return err
			}

			query = query.AddRoleIDs(req.RoleIds...)
		}

		return query.Exec(l.ctx)

	})

	if err != nil {
		return nil, dberrorhandler.DefaultEntError(l.Logger, err, req)
	}

	return &types.BaseMsgResp{Msg: "success"}, nil
}
