package svc

import (
	"firestream/api/internal/config"
	"firestream/api/internal/middleware"
	"firestream/ent"
	"firestream/rpc/coreClient"

	"github.com/casbin/casbin/v2"
	"github.com/redis/go-redis/v9"
	"github.com/zeromicro/go-zero/core/logx"
	"github.com/zeromicro/go-zero/rest"
)

type ServiceContext struct {
	Config    config.Config
	Authority rest.Middleware
	Casbin    *casbin.Enforcer
	Redis     redis.UniversalClient
	DB        *ent.Client
	CoreRpc   coreclient.Core
}

func NewServiceContext(c config.Config) *ServiceContext {
	rds := c.RedisConf.MustNewUniversalRedis()

	cbn := c.CasbinConf.MustNewCasbinWithOriginalRedisWatcher(c.DatabaseConf.Type, c.DatabaseConf.GetDSN(),
		c.RedisConf)

	db := ent.NewClient(
		ent.Log(logx.Info), // logger
		ent.Driver(c.DatabaseConf.NewNoCacheDriver()),
		ent.Debug(), // debug mode
	)

	return &ServiceContext{
		Config:    c,
		CoreRpc:   coreclient.NewCore(zrpc.NewClientIfEnable(c.CoreRpc)),
		Redis:     rds,
		Casbin:    cbn,
		DB:        db,
		Authority: middleware.NewAuthorityMiddleware(cbn, rds).Handle,
	}
}
