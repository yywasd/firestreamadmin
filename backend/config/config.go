package config

import (
	"fmt"
	"os"

	"github.com/joho/godotenv"
	"github.com/spf13/viper"
)

var ConfigFile Config

type Config struct {
	Path string
	Type string
}

func (c Config) InitConfig() error {
	viper.SetConfigFile(c.Path)
	viper.SetConfigType(c.Type)
	viper.AutomaticEnv()
	err := viper.ReadInConfig()
	if err != nil {
		fmt.Print("Error loading .env file")
		return err
	}
	return nil
}

// Config function to get value from env file
// perhaps not the best implementation
func ConfigGodotenv(key string) string {
	// load .env file
	err := godotenv.Load(".env")
	if err != nil {
		fmt.Print("Error loading .env file")
	}
	return os.Getenv(key)
}

func (c Config) GetValue(key string) string {
	return viper.GetString(key)
}
