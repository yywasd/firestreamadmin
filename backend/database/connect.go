package database

import (
	"context"
	"firestream/ent"
	"log"

	_ "github.com/mattn/go-sqlite3"
)

var DB *ent.Client

// ConnectDB connect to db
func ConnectDB() error {
	var err error
	DB, err = ent.Open("sqlite3", "file:ent.db?cache=shared&_fk=1")
	if err != nil {
		log.Fatalf("failed opening connection to sqlite: %v", err)
		return err
	}
	// defer DB.Close()
	// Run the auto migration tool.
	if err := DB.Schema.Create(context.Background()); err != nil {
		log.Fatalf("failed creating schema resources: %v", err)
		return err
	}

	return nil
}
