package mixins

import (
	"time"

	"entgo.io/ent/dialect/entsql"

	"entgo.io/ent"
	"entgo.io/ent/schema/field"
	"entgo.io/ent/schema/mixin"
)

// IDUint32Mixin is the mixin with Uint32 type ID field
// and the created_at, updated_at fields.
type IDUint32Mixin struct {
	mixin.Schema
}

func (IDUint32Mixin) Fields() []ent.Field {
	return []ent.Field{
		field.Uint32("id"),
		field.Time("created_at").
			Immutable().
			Default(time.Now).
			Comment("Create Time | 创建日期").
			Annotations(entsql.WithComments(true)),
		field.Time("updated_at").
			Default(time.Now).
			UpdateDefault(time.Now).
			Comment("Update Time | 修改日期").
			Annotations(entsql.WithComments(true)),
	}
}
