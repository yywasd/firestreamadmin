package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/dialect/entsql"
	"entgo.io/ent/schema"
	"entgo.io/ent/schema/field"

	"firestream/api2/ent/schema/mixins"
)

type Rule struct {
	ent.Schema
}

func (Rule) Fields() []ent.Field {
	return []ent.Field{
		field.String("type").
			Comment("ptype | 策略类型").
			Annotations(entsql.WithComments(true)),
		field.String("v0").
			Comment("v0 | 角色码，用于前端权限控制").
			Annotations(entsql.WithComments(true)),
		field.String("v1").Comment("v1 | 菜单码，用于后端权限控制").Annotations(entsql.WithComments(true)),
		field.String("v2").Comment("v2 | 菜单码，用于后端权限控制").Annotations(entsql.WithComments(true)),
		field.String("v3").Comment("v3 | 菜单码，用于后端权限控制").Annotations(entsql.WithComments(true)),
		field.String("v4").Comment("v4 | 菜单码，用于后端权限控制").Annotations(entsql.WithComments(true)),
		field.String("v5").Comment("v5 | 菜单码，用于后端权限控制").Annotations(entsql.WithComments(true)),
	}
}

func (Rule) Mixin() []ent.Mixin {
	return []ent.Mixin{
		mixins.UUIDMixin{},
	}
}

func (Rule) Annotations() []schema.Annotation {
	return []schema.Annotation{
		entsql.Annotation{Table: "sys_rules"},
	}
}
