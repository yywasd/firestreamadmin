package handler

import (
	"firestream/database"
	"firestream/ent"

	"github.com/gofiber/fiber/v2"
)

func GetPolicy(c *fiber.Ctx) error {
	db := database.DB
	rules, err := db.CasbinRule.Query().All(c.Context())
	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"message": err.Error(),
		})
	}
	return c.JSON(rules)
}

// create a new policy
func CreatePolicy(c *fiber.Ctx) error {
	db := database.DB
	var policy = new(ent.CasbinRule)
	if err := c.BodyParser(policy); err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"message": err.Error(),
		})
	}
	createdPolicy, err := db.CasbinRule.Create().SetPtype(policy.Ptype).SetV0(policy.V0).SetV1(policy.V1).SetV2(policy.V2).Save(c.Context())
	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"message": err.Error(),
		})
	}
	return c.JSON(createdPolicy)
}
