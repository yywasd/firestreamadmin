package handler

import (
	"context"
	"fmt"

	// "database/sql"

	"firestream/database"
	"firestream/ent"
	"firestream/ent/user"

	"github.com/gofiber/fiber/v2"
	"github.com/golang-jwt/jwt/v5"
	"golang.org/x/crypto/bcrypt"

	"firestream/utils/uuidx"
)

func hashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}

func validToken(t *jwt.Token, id string) bool {
	claims := t.Claims.(jwt.MapClaims)
	uid := claims["user_id"]

	return uid == id
}

func validUser(id string, p string) bool {
	db := database.DB
	ctx := context.Background()
	user, err := db.User.Query().Where(user.IDEQ(uuidx.ParseUUIDString(id))).Only(ctx)

	if err != nil {
		return false
	}
	if user.Username == "" {
		return false
	}
	if !CheckPasswordHash(p, user.Password) {
		return false
	}
	return true
}

// GetUser get a user
func GetUser(c *fiber.Ctx) error {
	id := c.Params("id")
	db := database.DB
	user, err := db.User.Query().Where(user.IDEQ(uuidx.ParseUUIDString(id))).Only(c.Context())

	if err != nil {
		return err
	}
	if user.Username == "" {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No user found with ID", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "User found", "data": user})
}

// GetUsers get users
func GetUsers(c *fiber.Ctx) error {
	username := c.Params("username")
	db := database.DB
	// var s *sql.Selector
	// qf := dbtool.QueryLike(s, map[string]string{"username": username})
	// users, err := db.User.Query().Where(qf).All(c.Context())

	users, err := db.User.Query().Where(user.Or(user.UsernameContains(username))).All(c.Context())
	if err != nil {
		return err
	}

	if len(users) == 0 {
		return c.Status(404).JSON(fiber.Map{"status": "error", "message": "No user found", "data": nil})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "User found", "data": users})
}

// CreateUser new user
func CreateUser(c *fiber.Ctx) error {
	db := database.DB
	user := new(ent.User)
	if err := c.BodyParser(user); err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Review your input", "data": err})

	}

	hash, err := hashPassword(user.Password)
	if err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Couldn't hash password", "data": err})

	}

	u, err := db.User.Create().SetUsername(user.Username).SetPassword(hash).SetNickname(user.Nickname).Save(c.Context())
	fmt.Println(err)
	if err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Couldn't create user^^^", "data": err})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "Created user", "data": u})
}

// UpdateUser update user
func UpdateUser(c *fiber.Ctx) error {
	var uui = new(ent.User)
	if err := c.BodyParser(&uui); err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Review your input", "data": err})
	}
	id := c.Params("id")
	token := c.Locals("user").(*jwt.Token)

	if !validToken(token, id) {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Invalid token id", "data": nil})
	}

	db := database.DB
	_, err := db.User.Update().Where(user.IDEQ(uuidx.ParseUUIDString(id))).SetEmail(uui.Email).Save(c.Context())
	if err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Couldn't update user", "data": err})
	}

	return c.JSON(fiber.Map{"status": "success", "message": "User successfully updated"})
}

// DeleteUser delete user
func DeleteUser(c *fiber.Ctx) error {
	type PasswordInput struct {
		Password string `json:"password"`
	}
	var pi PasswordInput
	if err := c.BodyParser(&pi); err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Review your input", "data": err})
	}
	id := c.Params("id")
	token := c.Locals("user").(*jwt.Token)

	if !validToken(token, id) {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Invalid token id", "data": nil})

	}

	if !validUser(id, pi.Password) {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Not valid user", "data": nil})

	}

	db := database.DB
	err := db.User.DeleteOneID(uuidx.ParseUUIDString(id)).Exec(c.Context())
	if err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Couldn't delete user", "data": err})
	}
	return c.JSON(fiber.Map{"status": "success", "message": "User successfully deleted", "data": nil})
}
