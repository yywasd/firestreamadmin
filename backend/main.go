package main

import (
	"firestream/database"
	"firestream/router"

	"github.com/gofiber/fiber/v2"
	_ "github.com/mattn/go-sqlite3"
)

func main() {
	err := database.ConnectDB()
	if err != nil {
		panic(err)
	}
	app := fiber.New()
	router.SetupRoutes(app)
	app.Listen(":3000")
}
