package config

// RedisCaptchaPrefix is the prefix of captcha key in redis
const RedisCaptchaPrefix = "CAPTCHA:"

// RedisTokenPrefix is the prefix of blacklist token key in redis
const RedisTokenPrefix = "BLACKLIST:TOKEN:"

// RedisCasbinChannel is the channel of captcha key in redis
const RedisCasbinChannel = "/casbin"

// RedisApiPermissionCountPrefix is the prefix of api permission access times left in redis
const RedisApiPermissionCountPrefix = "API:PERMISSION:"
