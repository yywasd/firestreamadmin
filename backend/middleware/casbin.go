package middleware

import (
	entadapter "github.com/casbin/ent-adapter"
	"github.com/gofiber/contrib/casbin"
	"github.com/gofiber/fiber/v2"
	"github.com/golang-jwt/jwt/v5"
	// casbinn "github.com/casbin/casbin/v2"
)

var authz *casbin.Middleware
var err error

func init() {
	authz, err = NewCasbin()
	if err != nil {
		panic(err)
	}
}

func NewCasbin() (*casbin.Middleware, error) {
	// adapter, err := entadapter.NewAdapter(config.ConfigFile.GetValue("casbin.dbType"), config.ConfigFile.GetValue("casbin.dsn"))
	adapter, err := entadapter.NewAdapter("sqlite3", "file:/home/dsjzx/Projects/firestream/backend/ent.db?cache=shared&_fk=1")
	if err != nil {
		return nil, err
	}
	return casbin.New(
		casbin.Config{
			// ModelFilePath: config.ConfigFile.GetValue("casbin.model_file_path"),
			ModelFilePath: "/home/dsjzx/Projects/firestream/backend/resource/casbin/rbac_model.conf",
			PolicyAdapter: adapter,
			Lookup: func(c *fiber.Ctx) string {
				if c.Locals("user") == nil {
					return ""
				}
				token := c.Locals("user").(*jwt.Token)
				if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
					return claims["username"].(string)
				}
				return ""
			},
		},
	), nil
}

func CasbinProtected() fiber.Handler {
	return authz.RoutePermission()
}
