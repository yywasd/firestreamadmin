package casbin

import (
	"firestream/config"

	entadapter "github.com/casbin/ent-adapter"
	"github.com/gofiber/contrib/casbin"
	"github.com/gofiber/fiber/v2"
)

func NewCasbin(dbType, dsn string) (*casbin.Middleware, error) {
	adapter, err := entadapter.NewAdapter(config.ConfigFile.GetValue("casbin.dbType"), config.ConfigFile.GetValue("casbin.dsn"))
	if err != nil {
		return nil, err
	}

	return casbin.New(
		casbin.Config{
			ModelFilePath: config.ConfigFile.GetValue("casbin.model_file_path"),
			PolicyAdapter: adapter,
			Lookup: func(c *fiber.Ctx) string {
				return ""
			},
		},
	), nil
}
