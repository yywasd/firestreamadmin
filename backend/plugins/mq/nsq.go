package mq

import (
	"fmt"
	"github.com/nsqio/go-nsq"
)

type NSQ struct {
	nsq.Producer
}

type NSQConsumer struct {
	nsq.Consumer
}

func NewNSQ(addr string) (*NSQ, error) {
	config := nsq.NewConfig()
	config.UserAgent = "nsq-go-client/1.0.0"
	config.MaxInFlight = 100
	config.MaxBackoffDuration = 5
	config.MaxRequeueDelay = 5
	config.MaxAttempts = 3
	config.DefaultRequeueDelay = 5
}

func NewNSQConsumer(addr string, topic string, channel string, handler nsq.Handler) (*NSQConsumer, error) {
	config := nsq.NewConfig()
	config.UserAgent = "nsq-go-client/1.0.0"
	config.MaxInFlight = 100
	config.MaxBackoffDuration = 5
	config.MaxRequeueDelay = 5
	config.MaxAttempts = 3
	config.DefaultRequeueDelay = 5
	consumer, err := nsq.NewConsumer(topic, channel, config)
	if err != nil {
		return nil, err
	}
	consumer.AddHandler(handler)
	if err := consumer.ConnectToNSQLookupd(addr); err != nil {
		return nil, err
	}
	return &NSQConsumer{*consumer}, nil
}

func (n *NSQConsumer) Close() {
	n.Consumer.Stop()

}

func (n *NSQ) Publish(topic string, body []byte) error {
	return n.Producer.Publish(topic, body)
}

func (n *NSQ) PublishAsync(topic string, body []byte, handler nsq.Handler) error {
	return n.Producer.PublishAsync(topic, body, handler)
}

func (n *NSQ) PublishWithDelay(topic string, body []byte, delay int64) error {
	return n.Producer.PublishWithDelay(topic, body, delay)
}

func (n *NSQ) PublishWithDelayAsync(topic string, body []byte, delay int64, handler nsq.Handler) error {
	return n.Producer.PublishWithDelayAsync(topic, body, delay, handler)
}

func (n *NSQ) PublishWithDelayAndRetry(topic string, body []byte, delay int64, retry int) error {
	for i := 0; i < retry; i++ {
		if err := n.PublishWithDelay(topic, body, delay); err != nil {
			return err
		}
	}
	return nil
}
func (n *NSQ) PublishWithDelayAndRetryAsync(topic string, body []byte, delay int64, retry int, handler nsq.Handler) error {
	for i := 0; i < retry; i++ {
		if err := n.PublishWithDelayAsync(topic, body, delay, handler); err != nil {
			return err
		}
	}
}

// # HandleMessage
type NSQHandler struct {
	Handler                func(message *nsq.Message) error
	Topic                  string
	Channel                string
	Addr                   string
	MaxInFlight            int
	MaxAttempts            int
	MaxRequeueDelay        int
	MaxBackoffDuration     int
	DefaultRequeueDelay    int
	MaxInFlightPerConsumer int
}

func (h *NSQHandler) HandleMessage(m *nsq.Message) error {

	if len(m.Body) == 0 {
		return nil
	}

	// do whatever actual message processing is desired
	err := processMessage(m.Body)

	// Returning a non-nil error will automatically send a REQ command to NSQ to re-queue the message.
	return err
}

func ConsumeMessage(c *nsq.Consumer, h *NSQHandler, LookupdAddr string, sigChan chan string) {
	c.AddHandler(h)
	err = c.ConnectToNSQLookupd(LookupdAddr)
	if err != nil {
		log.Fatal(err)
	}
	<-sigChan
	consumer.Stop()
}
