package minio

import (
	"context"
	"io"

	"firestream/config"
	minio "github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
)

func NewMinio(c config.MinioConf) (*minio.Client, error) {
	return minio.New(c.Endpoint, &minio.Options{
		Creds:  credentials.NewStaticV4(c.AccessKeyID, c.SecretAccessKey, ""),
		Secure: c.UseSSL,
	})
}

// # Bucket是否存在

func BucketExists(c *minio.Client, bucketName string) (bool, error) {
	return c.BucketExists(context.Background(), bucketName)
}

// # 存储文件
func FUploadFile(c *minio.Client, bucketName, objectName, filePath string) error {
	_, err := c.FPutObject(context.Background(), bucketName, objectName, filePath, minio.PutObjectOptions{})
	return err
}

// # 存储object
func UploadObject(c *minio.Client, bucketName, objectName string, data io.Reader) error {
	_, err := c.PutObject(context.Background(), bucketName, objectName, data, data.status().size(), minio.PutObjectOptions{})
	return err
}

// # 获取object
func GetObject(c *minio.Client, bucketName, objectName string) (io.Reader, error) {
	object, err := c.GetObject(context.Background(), bucketName, objectName, minio.GetObjectOptions{})
	return object, err
}

// # 获取文件
func GetFile(c *minio.Client, bucketName, objectName, filePath string) error {
	return c.FGetObject(context.Background(), "mybucket", "myobject", "/tmp/myobject", minio.GetObjectOptions{})
}
