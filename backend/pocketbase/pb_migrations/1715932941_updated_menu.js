/// <reference path="../pb_data/types.d.ts" />
migrate((db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("va13uoej1d7w18x")

  // add
  collection.schema.addField(new SchemaField({
    "system": false,
    "id": "bvss5h3a",
    "name": "parent_id",
    "type": "text",
    "required": false,
    "presentable": false,
    "unique": false,
    "options": {
      "min": null,
      "max": null,
      "pattern": ""
    }
  }))

  return dao.saveCollection(collection)
}, (db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("va13uoej1d7w18x")

  // remove
  collection.schema.removeField("bvss5h3a")

  return dao.saveCollection(collection)
})
