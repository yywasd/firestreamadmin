/// <reference path="../pb_data/types.d.ts" />
migrate((db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("ujdkmtag7uxa5bg")

  // add
  collection.schema.addField(new SchemaField({
    "system": false,
    "id": "5unqdltj",
    "name": "time",
    "type": "date",
    "required": false,
    "presentable": false,
    "unique": false,
    "options": {
      "min": "",
      "max": ""
    }
  }))

  return dao.saveCollection(collection)
}, (db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("ujdkmtag7uxa5bg")

  // remove
  collection.schema.removeField("5unqdltj")

  return dao.saveCollection(collection)
})
