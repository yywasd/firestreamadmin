package dbtool

import (
	"entgo.io/ent/dialect/sql"
)

// ent 构建模糊查询
func QueryLike(s *sql.Selector, cv map[string]string) func(s *sql.Selector) {
	var predicates []*sql.Predicate
	for k, v := range cv {
		predicates = append(predicates, sql.Like(s.C(k), "%"+v+"%"))
	}
	return func(s *sql.Selector) {
		s.Where(sql.Or(predicates...))
	}
}
