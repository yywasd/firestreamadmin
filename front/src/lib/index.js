import { goto } from '$app/navigation';
import PocketBase from 'pocketbase';
import { writable } from 'svelte/store';

export const isdark = writable(false);
export const activeMenuItem = writable();
export const pb = new PocketBase('http://10.155.73.154:10001/db');
export const collection = writable();


export function buildDirectoryTree(data) {
	const tree = [];
	function recursiveBuild(node, items) {
		items.forEach((item) => {
			if (item.parent_id === node.id) {
				const newNode = { ...item };
				newNode.children = [];
				node.children.push(newNode);
				recursiveBuild(newNode, items);
			}
		});
	}

	data.forEach((item) => {
		if (!item.parent_id) {
			tree.push({ ...item, children: [] });
			recursiveBuild(tree[tree.length - 1], data);
		}
	});

	return tree;
}

export async function login(username, password) {
	const authData = await pb.admins.authWithPassword(username, password);
	if (authData.code === 400) {
		alert('login failed');
		return;
	}
	goto('/admin');
}
