/** @type {import('./$types').LayoutLoad} */
export async function load() {
    return {};
}

export const ssr = false;
export const prerender = true;
// 'never,always,ignore'
export const trailingSlash = 'never';
