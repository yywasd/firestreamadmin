import { pb } from '$lib/index'
import { redirect } from '@sveltejs/kit';


/** @type {import('./$types').LayoutLoad} */
export async function load() {
    if (!pb.authStore.isValid) {
        redirect(302, '/login');
    }
    
    const resultList = await pb.collection('menu').getFullList();
    return {
        pb: pb,
        menulist: resultList,
    };
}